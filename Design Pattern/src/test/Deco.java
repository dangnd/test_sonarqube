package test;

public abstract class Deco implements Shape {
	protected Shape shape;
	public Deco(Shape shape) {
		this.shape = shape;
	}
	public String draw() {
		return shape.draw();
	}
}
