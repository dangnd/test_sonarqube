package vn.rurnsystem.command.oder.stock;

public class SellStock implements Oder {
	private Stock stock;
	public SellStock( Stock stock) {
		this.stock = stock;
	}
	@Override
	public void excute() {
		// TODO Auto-generated method stub
		stock.sell();
	}

}
