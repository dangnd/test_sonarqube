package vn.rurnsystem.command.oder.stock;

public class BuyStock implements Oder{
	private Stock stock;
	public BuyStock( Stock stock) {
		this.stock = stock;
	}
	@Override
	public void excute() {
		// TODO Auto-generated method stub
		stock.buy();
	}

}
