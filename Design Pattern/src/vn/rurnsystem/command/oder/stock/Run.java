package vn.rurnsystem.command.oder.stock;


public class Run {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stock stock = new Stock("hang",1);
		BuyStock buy = new BuyStock(stock);
		SellStock sell = new SellStock(stock);
		Broker brocker = new Broker();
		brocker.takeOder(buy);
		brocker.takeOder(sell);
		brocker.placeOder();
		
	}

}
