package vn.rurnsystem.command.oder.stock;

import java.util.ArrayList;
import java.util.List;

public class Broker {
	private List<Oder> orderList = new ArrayList<Oder>();
	public void takeOder(Oder oder) {
		orderList.add(oder);
	}
	public void placeOder() {
		for(Oder order: orderList) {
			order.excute();
		}
		orderList.clear();
	}
}
