package vn.rurnsystem.command.oder.stock;

public class Stock {
	private String name;
	private int quantity;
	public Stock(String name, int quantity) {
		this.setName(name);
		this.setQuantity(quantity);
	}
	public void buy() {
		System.out.print(" buy " + this.getName() + this.getQuantity());
	}
	public void sell() {
		System.out.print(" sell "+ this.getName() + this.getQuantity());
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
