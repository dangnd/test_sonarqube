package vn.rurnsystem.command.oder;

public class Waiter implements Oder {
	private Oder pizzaOder;
	private Oder saladOder;
	private String type;
	public Waiter(String type) {
		this.type = type;
	}
	@Override
	public void excute() {
		// TODO Auto-generated method stub
		if (type.equalsIgnoreCase("pizza")) {
			pizzaOder = new PizzaOder();
			pizzaOder.excute();
		}else if (type.equalsIgnoreCase("salad")) {
			saladOder = new SaladOder();
			saladOder.excute();
		}else {
			System.out.print("this type is not valid");
		}
		
	}

}
