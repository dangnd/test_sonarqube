package vn.runsystem.abstractfactorypattern;

public interface Color {
	public void fill();
}
