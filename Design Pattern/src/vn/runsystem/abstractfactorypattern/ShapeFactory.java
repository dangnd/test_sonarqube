package vn.runsystem.abstractfactorypattern;

import java.util.Scanner;

public class ShapeFactory extends AbstractFactory<Shape> {

	@Override
	public Shape creat(String name) {
		// TODO Auto-generated method stub
		if (name.equals("circle")) return new Circle();
		if (name.equals("square")) return new Square(); 
		return null;
	}

}
