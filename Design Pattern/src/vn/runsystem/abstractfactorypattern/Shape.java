package vn.runsystem.abstractfactorypattern;

public interface Shape {
	public void draw();
	public void getDescription();
}
