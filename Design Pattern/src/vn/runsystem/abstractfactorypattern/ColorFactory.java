package vn.runsystem.abstractfactorypattern;

public class ColorFactory extends AbstractFactory<Color> {

	@Override
	public Color creat(String name) {
		// TODO Auto-generated method stub
		if (name.equals("red")) return new Red();
		if (name.equals("blue")) return new Blue();
		return null;
	}
}
