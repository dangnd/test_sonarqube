package vn.runsystem.abstractfactorypattern;

public class FactoryProvider {
	public static AbstractFactory factoryProvider(String type) {
		if (type.equals("shape")) return new ShapeFactory();
		if (type.equals("color")) return new ColorFactory();	
		return null; 
	}
}
