package vn.runsystem.singleton;

public class SingleObject {
	private static SingleObject instance;
	protected SingleObject() {
	}
	public static SingleObject getObject() {
		if (instance==null) {
			synchronized(SingleObject.class) {
				if (instance==null) instance = new SingleObject();
			}
		}
		return instance;
	}
	public void showMessage() {
		System.out.print("this is singleton object");
	}
}
