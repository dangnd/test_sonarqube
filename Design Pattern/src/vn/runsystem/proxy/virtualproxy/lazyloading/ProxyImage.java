package vn.runsystem.proxy.virtualproxy.lazyloading;

public class ProxyImage implements Image {
	public RealImage realImage;
	public String fileName;
	
	public ProxyImage(String fileName) {
		this.fileName = fileName;
	}
	@Override
	public void display() {
		// TODO Auto-generated method stub
		if (realImage == null) {
			realImage = new RealImage(fileName);
		}
		realImage.display();
	}

}
