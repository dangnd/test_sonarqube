package vn.runsystem.proxy.virtualproxy.lazyloading;

public interface Image {
	public void display();
}
