package vn.runsystem.proxy.virtualproxy.lazyloading;

public class RealImage implements Image {
	public String fileName;
	public RealImage(String fileName) {
		this.fileName = fileName;
		loadFromDisk(fileName);
	}
	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("Display "+fileName);
	}
	public void loadFromDisk(String fileName) {
		System.out.print("Loading " +fileName );
	}

}
