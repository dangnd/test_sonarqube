package vn.runsystem.proxy.protectedproxy;

public class RealUserService implements UserService{
	private String name;
	public RealUserService(String name) {
		this.name = name;
	}
	
	@Override
	public void load() {
		// TODO Auto-generated method stub
		System.out.print(name + " loaded\n");
	}

	@Override
	public void insert() {
		// TODO Auto-generated method stub
		System.out.print(name + " inserted\n");
	}

}
