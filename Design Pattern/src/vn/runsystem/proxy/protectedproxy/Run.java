package vn.runsystem.proxy.protectedproxy;

public class Run {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserService admin = new Proxy("dang","admin");
		admin.load();
		admin.insert();
		UserService customer = new Proxy("dang","customer");
		customer.load();
		customer.insert();
	}

}
