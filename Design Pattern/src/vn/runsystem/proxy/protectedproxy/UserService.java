package vn.runsystem.proxy.protectedproxy;

public interface UserService {
	public void load();
	public void insert();
}
