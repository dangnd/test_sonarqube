package vn.runsystem.proxy.protectedproxy;

public class Proxy implements UserService{
	private String role;
	private RealUserService userService;
	public Proxy(String name , String role) {
		this.role = role;
		userService = new RealUserService(name);
	}
	@Override
	public void load() {
		// TODO Auto-generated method stub
		userService.load();
	}

	@Override
	public void insert() {
		// TODO Auto-generated method stub
		if (isAdmin()) {
			userService.insert();
		}else {
			System.out.print("access error");
		}
	}
	private boolean isAdmin() {
		return this.role.equalsIgnoreCase("admin");
	}

}
