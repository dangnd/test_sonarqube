package vn.runsystem.factorypattern.shape;

public class ShapeFactory {
	public static Shape getShape(String shape) {
		if (shape.equals("circle")) {
			return new Circle();
		}		
		if (shape.equals("square")) {
			return new Square();
		}
		return null;
	}
}
