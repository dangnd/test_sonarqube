package vn.runsystem.factorypattern.shape;

import javax.swing.JOptionPane;

public class Run {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String input = JOptionPane.showInputDialog("nhap vao hinh ban muon ve");
		Shape shape = ShapeFactory.getShape(input);
		JOptionPane.showMessageDialog(null, shape.draw());
	}
}
