package vn.runsystem.factorypattern.shape;

public interface Shape {
	public String draw();
}
