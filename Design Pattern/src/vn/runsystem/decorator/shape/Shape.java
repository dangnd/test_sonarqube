package vn.runsystem.decorator.shape;

public interface Shape {
	public void draw();
}
