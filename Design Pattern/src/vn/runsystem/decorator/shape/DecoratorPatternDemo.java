package vn.runsystem.decorator.shape;

public class DecoratorPatternDemo {
	public static void main(String[] arg) {
		Shape circle = new Circle();
		circle.draw();
		System.out.print("\n");
		RedShapeDecorator red = new RedShapeDecorator(circle);
		red.draw();
		System.out.print("\n");
		BackgroundShapeDecorator bgr = new BackgroundShapeDecorator(red);
		bgr.draw();
	}
}

