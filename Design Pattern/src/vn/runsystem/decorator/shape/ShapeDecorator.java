package vn.runsystem.decorator.shape;

public abstract class ShapeDecorator implements Shape{
	public Shape shape;
	//constructor
	public ShapeDecorator(Shape shape) {
		this.shape = shape;
	}
	public void draw() {
		shape.draw();
	}
}
