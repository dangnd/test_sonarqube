package vn.runsystem.facade;

public class Run {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ShapeMaker shapeMaker = ShapeMaker.getInstance();
		shapeMaker.drawCircle();
		shapeMaker.drawSquare();
	}

}
