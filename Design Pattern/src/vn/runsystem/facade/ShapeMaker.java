package vn.runsystem.facade;

public class ShapeMaker {
	private Circle circle;
	private Square square;
	private static ShapeMaker instance;
	private ShapeMaker() {
		circle = new Circle();
		square = new Square();
	}
	public static ShapeMaker getInstance() {
		if (instance == null) {
			synchronized(ShapeMaker.class) {
				instance = new ShapeMaker();
			}
		}
		return instance;
	}
	public void drawCircle() {
		circle.draw();
	}
	public void drawSquare() {
		square.draw();
	}
	
}
