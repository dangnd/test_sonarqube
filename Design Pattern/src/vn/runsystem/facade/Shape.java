package vn.runsystem.facade;

public interface Shape {
	public void draw();
}
